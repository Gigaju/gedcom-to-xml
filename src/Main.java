import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main {
	
	public static void main(String[] args){
		FileInputStream fis = null;
		Traducteur tr = null;
		if(args.length == 0 || args[0].equals("--help")){
			System.out.println("gedcomtoxml : aide");
			System.out.println("\tUTILISATION : gedcomtoxml INPUTFILENAME");
			System.out.println("\tCe programme convertit des fichiers Gedcom 5 valides en fichiers XML.");
			System.exit(0);
		}
		else if(args.length == 1){
			try {
				fis = new FileInputStream(args[0]);
			} catch (FileNotFoundException e) {
				System.out.println("Erreur : "+args[0]+": fichier introuvable. utilisez l'argument --help pour afficher l'aide.");
			}
			tr = new Traducteur(fis);
		}
		else{
			System.out.println("Erreur : utilisez --help pour afficher l'aide.");
			System.exit(0);
		}
		tr.parse();
		tr.writeXML();
	}
}
