//Cette classe n'est pas très utile, tout aurait pu être mis dans le traducteur. Elle permet cependant, à mon avis, de rendre le code plus lisible.
public class Entree{
	private int niveau;
	private String id;
	private String tag;
	private String valeur;
	//private ArrayList<Entree> attributs; pas besoin

	public Entree(int niveau, String tag, String valeur){
		this.niveau = niveau;
		this.tag = tag;
		this.valeur = valeur;
		this.id = null;
		//this.attributs = new ArrayList<Entree>();
	}

	public Entree(int niveau, String id, String tag, String valeur){
		this(niveau, tag, valeur);
		this.id = id;
	}

	public int getNiveau(){
		return this.niveau;
	}

	public String getId(){
		return this.id;
	}

	public String getTag(){
		return this.tag;
	}

	public String getValeur(){
		return this.valeur;
	}
}
