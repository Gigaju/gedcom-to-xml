import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

//3 phases séparées : initialisation (constructeur), parsage (parse()) et traduction (writeXML()).
public class Traducteur {
	private BufferedReader br;
	private Document document;
	private ArrayList<Entree> entrees; //Contient toutes les entrées lues.

	public Traducteur(FileInputStream fis){
		this.br = new BufferedReader(new InputStreamReader(new DataInputStream(fis)));
		this.entrees = new ArrayList<Entree>();
		//Initialisation du nouveau document XML
		try{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			this.document = dbf.newDocumentBuilder().newDocument();
		}catch(ParserConfigurationException e){
			System.out.println("Erreur de configuration du parseur "+e);
			System.exit(1);
		}
	}

	//Ligne gedcom : niveau [@ID@] tag [valeur]
	public void parse(){
		//System.out.println("Parsage");
		String ligne;
		try {
			while((ligne = br.readLine()) != null){
				if(!ligne.equals("") && ligne.trim().length()!=0){ //Regex ajoutée a cause de royal.ged, ou il y a des lignes avec un nombre aléatoire d'espaces a ne pas prendre en compte.
					//System.out.println("ligne"); //debug
					String[] ligne_splittee=ligne.trim().split(" ");
					//for(int i = 0; i < ligne_splittee.length; i++)System.out.println(ligne_splittee[i]); //debug
					int niveau, debut_champ_valeur;
					String id=null, tag=null, valeur=null;
					niveau = Integer.parseInt(ligne_splittee[0]); //Champ niveau
					if(ligne_splittee[1].startsWith("@") && ligne_splittee[1].endsWith("@")){ //S'il y a un ID
						debut_champ_valeur = 3;
						id = ligne_splittee[1].replace("@", ""); //Champ ID -- suppression des @
						tag = ligne_splittee[2]; //Champ tag
					}
					else{
						debut_champ_valeur = 2;
						tag = ligne_splittee[1]; //Champ tag
					}
					StringBuilder sb = new StringBuilder();
					for(int i = debut_champ_valeur; i < ligne_splittee.length; i++){
						if (i != debut_champ_valeur) sb.append(" ");
						sb.append(ligne_splittee[i]);
					}
					if (sb.length() != 0) valeur = sb.toString(); //Champ valeur
					Entree resultat;
					if(id != null) resultat = new Entree(niveau, id, tag, valeur);
					else resultat = new Entree(niveau, tag, valeur);
					//derniere_entree_parsee[resultat.getNiveau()] = resultat; pas besoin
					this.entrees.add(resultat); //On stocke toutes les entrées dans une liste
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeXML(){
		//System.out.println("Traduction");
		Element root = this.document.createElement("gedcom");
		root.setAttribute("xsi:noNamespaceSchemaLocation", "gedcom.xsd");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		this.document.appendChild(root);
		Element[] dernier_element_traduit = new Element[15];//Contient le dernier élément traité pour chaque niveau. Permet à l'élément en cours de traitement de trouver son parent.
		dernier_element_traduit[0] = root; //0 : racine, 1 : niveau 0, 2 : niveau 1...
		for (Entree entree : entrees) {
			Element elem = null;
			switch (entree.getTag()){
			case "INDI":
				elem = this.document.createElement("indi");
				elem.setAttribute("id", entree.getId());
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "FAM":
				elem = this.document.createElement("fam");
				elem.setAttribute("id", entree.getId());
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "NAME":
				elem = this.document.createElement("name"); //Si il s'agit d'un nom
				elem.appendChild(this.document.createTextNode(entree.getValeur()));
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "TITL":
				elem = this.document.createElement("titl");
				elem.appendChild(this.document.createTextNode(entree.getValeur()));
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "SEX": 
				elem = this.document.createElement("sex");
				elem.setAttribute("s", entree.getValeur());
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "PLAC":
				elem = this.document.createElement("plac");
				elem.appendChild(this.document.createTextNode(entree.getValeur()));
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "DATE":
				elem = this.document.createElement("date");
				elem.appendChild(this.document.createTextNode(entree.getValeur()));
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "DIV":
				elem = this.document.createElement("div");
				if(entree.getValeur()!=null) elem.setAttribute("d", entree.getValeur());
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "BIRT":
				elem = this.document.createElement("birt");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "DEAT":
				elem = this.document.createElement("deat");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "BURI":
				elem = this.document.createElement("buri");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "BAPM":
				elem = this.document.createElement("bapm");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "MARR":
				elem = this.document.createElement("marr");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "CHR":
				elem = this.document.createElement("chr");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "FAMC":
				elem = this.document.createElement("famc");
				elem.setAttribute("ref", entree.getValeur().replace("@", ""));
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "FAMS":
				elem = this.document.createElement("fams");
				elem.setAttribute("ref", entree.getValeur().replace("@", ""));
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "HUSB":
				elem = this.document.createElement("husb");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				elem.setAttribute("ref", entree.getValeur().replace("@", ""));
				break;
			case "WIFE":
				elem = this.document.createElement("wife");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				elem.setAttribute("ref", entree.getValeur().replace("@", ""));
				break;
			case "CHIL":
				elem = this.document.createElement("chil");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				elem.setAttribute("ref", entree.getValeur().replace("@", ""));
				break;
			//Fin des tags à intégrer absolument
			//Début des tags supplémentaires associés à HEAD
			case "HEAD":
				elem = this.document.createElement("head");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "SOUR":
				elem = this.document.createElement("sour");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null){
					if(entree.getValeur().startsWith("@") && entree.getValeur().endsWith("@")) elem.setAttribute("ref", entree.getValeur().replace("@", ""));
					else elem.appendChild(this.document.createTextNode(entree.getValeur()));
				}
				if(entree.getId() != null) elem.setAttribute("id", entree.getId());
				break;
			case "VERS":
				elem = this.document.createElement("vers");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "CORP":
				elem = this.document.createElement("corp");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "DEST":
				elem = this.document.createElement("dest");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "FILE":
				elem = this.document.createElement("file");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "CHAR":
				elem = this.document.createElement("char");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "GEDC":
				elem = this.document.createElement("gedc");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "ADDR":
				elem = this.document.createElement("addr");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "PHON":
				elem = this.document.createElement("phon");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			//Début des tags supplémentaires "généraux"
			case "QUAY":
				elem = this.document.createElement("quay");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.setAttribute("q", entree.getValeur());
				break;
			case "TRLR":
				elem = this.document.createElement("trlr");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "SUBM":
				elem = this.document.createElement("subm");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getId() != null) elem.setAttribute("id", entree.getId());
				break;
			//Début des autres tags supplémentaires
			case "NUMB":
				elem = this.document.createElement("numb");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "ILLE":
				elem = this.document.createElement("ille");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "CHAN":
				elem = this.document.createElement("chan");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "MISC":
				elem = this.document.createElement("misc");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			case "OCCU":
				elem = this.document.createElement("occu");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "REFN":
				elem = this.document.createElement("refn");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "CONT":
				elem = this.document.createElement("cont");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				if(entree.getValeur() != null) elem.appendChild(this.document.createTextNode(entree.getValeur()));
				break;
			case "OBJE":
				elem = this.document.createElement("obje");
				dernier_element_traduit[entree.getNiveau()].appendChild(elem);
				break;
			default:
				//System.out.println(entree.getTag()+" : tag inconnu - ignoré."); //DECOMMENTER POUR VOIR LES TAGS IGNORES
				elem = this.document.createElement("dummy"); //On n'insère pas les éléments non traités mais on doit les prendre en compte.
			}
			dernier_element_traduit[entree.getNiveau()+1] = elem;
		}
		TransformerFactory trf = TransformerFactory.newInstance();
		try {
			Transformer tr = trf.newTransformer();
			DOMSource source = new DOMSource(this.document);
			StreamResult result;
			tr.setOutputProperty(OutputKeys.INDENT, "yes");
			tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "gedcom.dtd");
			result = new StreamResult(System.out);
			tr.transform(source, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
