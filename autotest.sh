#!/bin/bash
echo "Compilation du code source du traducteur :"
cd src && javac *.java
echo "Terminé"
echo "Traduction de doe.ged..."
java Main ../doe.ged > ../doe.xml
echo "Traduction de begood.ge..."
java Main ../begood.ged > ../begood.xml
echo "Traduction de complet.ged..."
java Main ../completutf8.ged > ../complet.xml
echo "Traduction de royal.ged..."
java Main ../royal.ged > ../royal.xml
echo "Traduction de french.ged..."
java Main ../french.ged > ../french.xml
cd ..
echo "- - - - - - DTD - - - - - -"
echo "Validation de doe.xml :"
xmllint --noout --dtdvalid gedcom.dtd doe.xml
echo "Validation de begood.xml :"
xmllint --noout --dtdvalid gedcom.dtd begood.xml
echo "Validation de complet.xml :"
xmllint --noout --dtdvalid gedcom.dtd complet.xml
echo "Validation de royal.xml :"
xmllint --noout --dtdvalid gedcom.dtd royal.xml
echo "Validation de french.xml :"
xmllint --noout --dtdvalid gedcom.dtd french.xml
echo "- - - - - Schéma - - - - -"
echo "Validation de doe.xml :"
xmllint -noout --schema gedcom.xsd doe.xml
echo "Validation de begood.xml :"
xmllint -noout --schema gedcom.xsd begood.xml
echo "Validation de complet.xml :"
xmllint -noout --schema gedcom.xsd complet.xml
echo "Validation de royal.xml :"
xmllint -noout --schema gedcom.xsd royal.xml
echo "Validation de french.xml :"
xmllint -noout --schema gedcom.xsd french.xml
echo "- - - - - - XSLT - - - - - "
echo "Traduction de doe.xml..."
xsltproc xml2html.xsl doe.xml
echo "Traduction de begood.xml..."
xsltproc xml2html.xsl begood.xml
echo "Traduction de complet.xml..."
xsltproc xml2html.xsl complet.xml
echo "Traduction de royal.xml..."
xsltproc xml2html.xsl royal.xml
echo "Traduction de french.xml..."
xsltproc xml2html.xsl french.xml
